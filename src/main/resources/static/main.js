//console.log("Hello you");
let data = [];
const state = {
    albumOpen: null,
    songOpen: null,
    addArtist: null,
    addAlbum: null,
    addTrack: null
}
var currentArtist = null
var currentArtistNo = null
var currentAlbum = null
var currentAlbumNo = null
refresh()

function refresh() {
    data = []
    fetch('./artists')
        .then(Response => Response.json())
        .then(newdata => {
            data = newdata
            render()
        })
        .catch(err => console.error(err));
    render()
}

function render() {
    let content = `
    <linkSection id="links">

    ${data.map((artistData, i) => {
        return `
            <item id="item" onclick="displayAlbums(${i})">
            <a style="text-decoration: none">
                <div class="linkImage" style="background-image: url(${artistData.imageURL} );">
                </div>
            </a>
            <div class="linkInfo">
                <p>${artistData.name}</p>
                <p>${artistData.rating}</p>
               

            </div>
            </item> 

            `
    }).join("")}
    <item id="item" onclick="getAddArtistFormHtml()">
    <a style="text-decoration: none">
        <div class="linkImage">
        </div>
    </a>
    <div class="linkInfo">
        <p></p>
        <p>Add Artist +</p>


    </div>
</item>
</linkSection>
    `
    const linksEl = document.getElementById('go')
    linksEl.innerHTML = content

    if (state.albumOpen) {
        const modalContent = `
            <section class="modal-bg" >
                <section class="modal-box">
                <section class="box-header"><div>${currentArtist.name+' Albums'}</div><button style="justify-content: right;" onclick="closeAll()">X</button></section>
                <section class="modal-grid">
                ${state.albumOpen.map((album, j) => {
                    return `
                    <article class="model-item" onclick="displaySongs(${j})">
                        <div class="box-Image" style="background-image: url(${album.albumImageURL} );">
                        </div>
                        <div class="box-link">
                        ${album.name}
                        ${album.rating}                       
                        </div>
                        </article>`
                    }).join("")}
                    <article class="model-item" onclick="getAddAlbumFormHtml()">
                        <div class="box-Image">
                        </div>
                        <div class="box-link">
                        ADD ALBUM                     
                        </div>
                        </article>                    
                    </section>
                    </section>
                
            </section>
        
        `
        const modalEl = document.getElementById('modal')
        modalEl.innerHTML = modalContent
    } else {
        const modalEl = document.getElementById('modal')
        modalEl.innerHTML = ""
    }

    if (state.songOpen) {
        const modalContent = `
            <section class="modal-bg" >
                <section class="modal-box">
                <section class="box-header-song"><div>${currentAlbum.name+' Tracks'}</div><div><button style="justify-content: right;" onclick="closeSongs()">back</button></div><div><button style="justify-content: right;" onclick="closeAll()">X</button></div></section>
                <table>
                <tr>
                          <th>Track No.</th>
                          <th>Track Name</th>
                          <th>Artist</th>
                          <th>Album</th>
                          <th>Rating</th>
                          <th>Duration</th>
                        </tr>
                ${state.songOpen.map(song => {
                    return `
                     <tr>
                          <td>${song.trackNumber}</td>
                          <td>${song.title}</td>
                          <td>${currentArtist.name}</td>
                          <td>${currentAlbum.name}</td>
                          <td>${song.rating}</td>
                          <td>${song.duration}</td>
                        </tr>                       
                      `
                    }).join("")}
                    <td onclick=" getAddTrackFormHtml();">add Track</td>
                    </table>
                    </section>
                    </section>
                
            </section>
        
        `
        const modalElSong = document.getElementById('songModal')
        modalElSong.innerHTML = modalContent
    } else {
        const modalEl = document.getElementById('songModal')
        modalEl.innerHTML = ""
    }
    

}

function displayAlbums(index){
    currentArtistNo = index
    currentArtist = data[index]
    console.log(currentArtist)
    state.albumOpen = currentArtist.albums
    render()
}

function displaySongs(index){
    currentAlbumNo = index
    currentAlbum = currentArtist.albums[index]
    console.log(index)
    state.songOpen = currentAlbum.songs
    render()
}
function closeAlbums(){
    state.albumOpen = null
    render()
}
function closeSongs(){
    state.songOpen = null
    render()
}
function closeAll(){
    state.albumOpen = null
    state.songOpen = null
    state.addArtist = null
    render()
}
function closeAddArtist(){
    state.addArtist = false
    const modalEladd = document.getElementById('addArtistModal')
        modalEladd.innerHTML = ""
    refresh()
    render()
}
function closeAddAlbum(){
    state.addAlbum = false
    const modalEladdAlbum = document.getElementById('addAlbumModal')
        modalEladdAlbum.innerHTML = ""
    refresh()
    render()
}
function closeAddTrack(){
    state.addTrack = false
    const modalEladdTrack = document.getElementById('addTrackModal')
        modalEladdTrack.innerHTML = ""
    refresh()
    render()
}
function closeConfirm(){
    state.addArtist = false
    state.addAlbum = false
    state.addTrack = false
    closeAll()
    const modalEl = document.getElementById('modal')
    modalEl.innerHTML = ""
    const modalElSong = document.getElementById('songModal')
    modalElSong.innerHTML = ""
    // state.songOpen = currentAlbum.songs
    // render()
    const modalElConfirm = document.getElementById('confirm')
    modalElConfirm.innerHTML = ""
   // displayAlbums(currentArtistNo)
    //displaySongs(currentAlbumNo)
}

function getAddArtistFormHtml() {
    state.addArtist = true
    if (state.addArtist) {
        const modalContentadd = 
        `
        <section class="modal-bg" >
            <section class="modal-box-add">
                <section class="box-header"><div>Add Artist</div><button style="justify-content: right;" onclick="closeAddArtist()">X</button></section>
                    <section class="additem">
        <article>
            <form onsubmit="event.preventDefault();addArtist(this);" >
                <label>Artist Name</label>
                <input name="name" required>
                <label>Artist Rating</label>
                <input name="rating" required>
                <label>Artist image</label>
                <input name="imageURL" type="url" required></label>
                    <button style="width:13rem;">Add Artist</button>
            </form>
            </article>
            </section></section></section>
        `
        const modalEladd = document.getElementById('addArtistModal')
        modalEladd.innerHTML = modalContentadd
        
    } else {
        const modalEladd = document.getElementById('addArtistModal')
        modalEladd.innerHTML = ""
    }
}

function addArtist(HTMLform){
    const data = new FormData(HTMLform)
    const name = data.get('name')
    const rating = data.get('rating')
    const imageURL = data.get('imageURL')
    fetch('/artists', {
        method: 'POST',
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify({name, rating, imageURL})
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
 
}



function getAddAlbumFormHtml() {
    state.addAlbum = true
    if (state.addAlbum) {
        const modalContentaddAlbum = 
        `
        <section class="modal-bg" >
            <section class="modal-box-add">
                <section class="box-header"><div>Add Album</div><button style="justify-content: right;" onclick="closeAddAlbum()">X</button></section>
                    <section class="additem">
        <article>
            <form onsubmit="event.preventDefault();addAlbum(this);" >
                <label>Album Name</label>
                <input name="name" required>
                <label>Artist Rating</label>
                <input name="rating" required>
                <label>Album image</label>
                <input name="albumImageURL" type="url" required></label>
                    <button style="width:13rem;">Add Album</button>
            </form>
            </article>
            </section></section></section>
        `
        const modalEladdAlbum = document.getElementById('addAlbumModal')
        modalEladdAlbum.innerHTML = modalContentaddAlbum
        
    } else {
        const modalEladdAlbum = document.getElementById('addAlbumModal')
        modalEladdAlbum.innerHTML = ""
    }
}
function addAlbum(HTMLform){
    const mapping = `/artists/${currentArtist.id}/albums`
    const data = new FormData(HTMLform)
    const name = data.get('name')
    const rating = data.get('rating')
    const albumImageURL = data.get('albumImageURL')
    fetch(mapping, {
        method: 'POST',
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify({name, rating, albumImageURL})
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
 
}

function getAddTrackFormHtml() {
    state.addTrack = true
    if (state.addTrack) {
        const modalContentaddTrack = 
        `
        <section class="modal-bg" >
            <section class="modal-box-add">
                <section class="box-header"><div>Add Track</div><button style="justify-content: right;" onclick="closeAddTrack()">X</button></section>
                    <section class="additem">
        <article>
            <form onsubmit="event.preventDefault();addTrack(this);" >
                <label>Track No.</label>
                <input name="trackNumber" required>
                <label>Track title</label>
                <input name="title" required>
                <label>Track duration</label>
                <input name="duration" required>
                <label>Track Rating</label>
                <input name="rating" required>
                    <button style="width:13rem;">Add Track</button>
            </form>
            </article>
            </section></section></section>
        `
        const modalEladdTrack = document.getElementById('addTrackModal')
        modalEladdTrack.innerHTML = modalContentaddTrack
        
    } else {
        const modalEladdTrack = document.getElementById('addTrackModal')
        modalEladdTrack.innerHTML = ""
    }
}

function addTrack(HTMLform){
    const mapping = `/artists/${currentArtist.id}/albums/${currentAlbum.id}/songs`
    const data = new FormData(HTMLform)
    const trackNumber = data.get('trackNumber')
    const title = data.get('title')
    const duration = data.get('duration')
    const rating = data.get('rating')
    
    fetch(mapping, {
        method: 'POST',
        headers: {
            "content-Type": "application/json"
        },
        body: JSON.stringify({trackNumber, title, duration, rating})
    }).then(res => res.json()).then(console.log).catch(console.error)
    confirm()
 
}

function confirm(){
    const modalElConfirmButt = 
        `
        <section class="modal-bg" >
            <section class="modal-box-confirm">
                <article style="justify-content: centre;">
                    <button style="justify-content: centre;" onclick="closeConfirm(); closeAddArtist(); closeAddAlbum(); closeAddTrack() ">added</button>
                </article>
            </section>
        </section>
        `

    const modalElConfirm = document.getElementById('confirm')
    modalElConfirm.innerHTML = modalElConfirmButt
}