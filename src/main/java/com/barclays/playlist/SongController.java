package com.barclays.playlist;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class SongController {
    private SongRepository repository;
    private AlbumRepository albumsRepository;

    SongController(SongRepository repository, AlbumRepository albumsRepository){
        this.repository=repository;
        this.albumsRepository=albumsRepository;
    }
    
    @PostMapping("/artists/{artist_id}/albums/{album_id}/songs")
    public Song newItem(@RequestBody Song songData, @PathVariable("artist_id") Integer artist_id,@PathVariable("album_id") Integer album_id){
        Album album = albumsRepository.findById(album_id).get();
        songData.setAlbum(album);
        return repository.save(songData);
    }
    @GetMapping("/artists/{artist_id}/albums/{album_id}/songs/{id}")
    public Song getOne(@PathVariable("artist_id") Integer artist_id, @PathVariable("album_id") Integer album_id,@PathVariable("id") Integer id){
        return repository.findById(id).get();
    }

    @PutMapping("/artists/{artist_id}/albums/{album_id}/songs/{id}")
    public Song updateOne(@RequestBody Song songUpdate, @PathVariable Integer restaurant_id, @PathVariable Integer menu_id, @PathVariable Integer id){
        Optional<Song> optionalMenu = repository.findById(id);
           try{ 
              Song song = optionalMenu.get();
              song.setTrackNumber(songUpdate.getTrackNumber());
              song.setTitle(songUpdate.getTitle());
              song.setDuration(songUpdate.getDuration());
              song.setRating(songUpdate.getRating());

            return repository.save(song);

           }catch(Exception err){
            throw new ResponseStatusException( HttpStatus.NOT_FOUND, "entity not found" );
           }        
    }
    @DeleteMapping("/artists/{artist_id}/albums/{album_id}/songs/{id}")
    public void deleteOne(@PathVariable("artist_id") Integer artist_id, @PathVariable("album_id") Integer album_id, @PathVariable("id") Integer id){
        repository.deleteById(id);
    }
    
}
