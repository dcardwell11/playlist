package com.barclays.playlist;

import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class AlbumController {
    private AlbumRepository repository;
    private ArtistRepository artistRepository;

    AlbumController(AlbumRepository repository, ArtistRepository artistRepository){
        this.repository = repository;
        this.artistRepository = artistRepository;
    }

    @PostMapping("/artists/{artist_id}/albums")
    public Album newMenu(@RequestBody Album albumData, @PathVariable Integer artist_id){
        Artist artist = artistRepository.findById(artist_id).get();
        albumData.setArtist(artist);
        return repository.save(albumData);
    }
    @GetMapping("/artists/{artist_id}/albums/{id}")
    public Album getOne(@PathVariable("artist_id") Integer artist_id, @PathVariable("id") Integer id){
        return repository.findById(id).get();
    }

    @PutMapping("/artists/{artist_id}/albums/{id}")
    public Album updateOne(@RequestBody Album albumUpdate, @PathVariable Integer artist_id, @PathVariable Integer id){
        Optional<Album> optionalAlbum = repository.findById(id);
           try{ 
              Album album = optionalAlbum.get();
              album.setName(albumUpdate.getName());
              album.setAlbumImageURL(albumUpdate.getAlbumImageURL());
              album.setRating(albumUpdate.getRating());

            return repository.save(album);

           }catch(Exception err){
            throw new ResponseStatusException( HttpStatus.NOT_FOUND, "entity not found" );
           }
    }
    
    @DeleteMapping("/artists/{artist_id}/albums/{id}")
    public void deleteOne(@PathVariable("artist_id") Integer artist_id, @PathVariable("id") Integer id){
        repository.deleteById(id);
    }
    
}
