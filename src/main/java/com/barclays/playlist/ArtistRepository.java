package com.barclays.playlist;

import org.springframework.data.jpa.repository.JpaRepository;

interface ArtistRepository extends JpaRepository<Artist, Integer> {

}
