package com.barclays.playlist;

import org.springframework.data.jpa.repository.JpaRepository;

interface SongRepository extends JpaRepository<Song, Integer> {
        
}
