package com.barclays.playlist;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ArtistController {
    private ArtistRepository repository;

    ArtistController(ArtistRepository repository){
        this.repository = repository;
    }    
    @GetMapping("/artists")
    public List<Artist> getArtist(){
        return repository.findAll();
    }
    @GetMapping("/artists/{id}")
    public Artist getOne(@PathVariable Integer id){
        return repository.findById(id).get();
    }
    @PostMapping("/artists")
    public Artist createArtist(@RequestBody Artist newArtist){
        return repository.save(newArtist);
    }
    @PutMapping("/artists/{id}")
    public Artist updateOne(@PathVariable Integer id, @RequestBody Artist updatedArtist){
        return repository.findById(id).map(artist -> {
            artist.setName(updatedArtist.getName());
            artist.setRating(updatedArtist.getRating());
            artist.setImageURL(updatedArtist.getImageURL());
            return repository.save(artist);
        }).orElseThrow();
    }
    @DeleteMapping("/artists/{id}")
    public void deleteOne(@PathVariable Integer id){
        repository.deleteById(id);
    }

    
    
}
